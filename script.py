#!/usr/bin/python

a1 = [
    [0, 1, 0, 0, 0, 0],
    [1, 0, 1, 1, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 1, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0]
]

a2 = [
    [0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 0, 1, 1],
    [0, 0, 1, 0, 0],
    [0, 0, 1, 0, 0]
]

a3 = [
    [0, 1, 0, 1, 0],
    [1, 0, 1, 0, 0],
    [0, 0, 0, 1, 1],
    [0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0]
]

a4 = [
    [0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0],
    [1, 0, 0, 0, 0],
    [0, 1, 0, 1, 0]
]

def get_maxR(a):
    maxR = []
    for i in range(0, len(a[0])):
        maxR.append(1)

    for i in range(0, len(a)):
        for j in range(0, len(a[0])):
            if a[i][j] == 1 and a[j][i] == 0:
                maxR[j] = 0

    for i in range(0, len(a)):
        for j in range(0, len(a[0])):
            if a[i][j] == a[j][i] == 1 and maxR[i] == 0:
                maxR[j] = 0
    return maxR
    

def print_maxR(maxR):
    for i, r in enumerate(maxR):
        print('x' + str(i + 1), end='\t')
    print('')

    for r in maxR:
        print(r, end='\t')
    print('')


def main():
    print('')
    maxR1 = get_maxR(a1)
    print('MaxR1(MaxR для a1):')
    print_maxR(maxR1)
    print('')

    maxR2 = get_maxR(a2)
    print('MaxR2(MaxR для a2):')
    print_maxR(maxR2)
    print('')

    maxR3 = get_maxR(a3)
    print('MaxR3(MaxR для a3):')
    print_maxR(maxR3)
    print('')

    maxR4 = get_maxR(a4)
    print('MaxR4(MaxR для a4):')
    print_maxR(maxR4)
    print('')


if __name__ == '__main__':
    main()